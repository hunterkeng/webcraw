const admin = require('firebase-admin');

var Crawler = require("crawler");

let serviceAccount = require('./test-3a7af-07099e487e21.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

let db = admin.firestore();

let host = 'https://www.health.govt.nz';
var c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server
            console.log($("title").text());
            let newCasesElement = $("tr:contains('confirmed and probable cases') td:nth-of-type(3)");
            let newCases = $(newCasesElement).text();

            let totalCasesElement = $("tr:contains('confirmed and probable cases') td:nth-of-type(2)");
            let totalCases = $(totalCasesElement).text();

            let recoveredCasesElement = $("tr:contains('recovered cases') td:nth-of-type(2)");
            let recoveredCases = $(recoveredCasesElement).text();

            let deathCasesElement = $("tr:contains('deaths') td:nth-of-type(2)");
            let deathCases = $(deathCasesElement).text();
            console.log(`newCases: ${newCases}`);
            console.log(`totalCases: ${totalCases}`);
            console.log(`recoveredCases: ${recoveredCases}`);
            console.log(`deathCases: ${deathCases}`);

            console.log("---------------------");


            let akl1CasesElement = $("tr:contains('Auckland') td:nth-of-type(2)");
            let akl1Cases = $(akl1CasesElement).text();

            let akl2CasesElement = $("tr:contains('Counties Manukau') td:nth-of-type(2)");
            let akl2Cases = $(akl2CasesElement).text();

            let akl3CasesElement = $("tr:contains('Waitemata') td:nth-of-type(2)");
            let akl3Cases = $(akl3CasesElement).text();

            let aucklandCases = parseInt(akl1Cases) + parseInt(akl2Cases) + parseInt(akl3Cases);
            console.log(`aucklandCases: ${aucklandCases}`);


            let bayOfPlentyCasesElement = $("tr:contains('Bay of Plenty') td:nth-of-type(2)");
            let bayofplentyCases = $(bayOfPlentyCasesElement).text();
            console.log(`bayofplentyCases: ${bayofplentyCases}`);


            let gisCasesElement = $("tr:contains('Tairāwhiti') td:nth-of-type(2)");
            let gisborneCases = $(gisCasesElement).text();
            console.log(`gisborneCases: ${gisborneCases}`);


            let hawkCasesElement = $("tr:contains('Hawke's Bay') td:nth-of-type(2)");
            let hawkesbayCases = $(hawkCasesElement).text();
            console.log(`hawkesbayCases: ${hawkesbayCases}`);


            let wanman1CasesElement = $("tr:contains('Whanganui') td:nth-of-type(2)");
            let wanman1Cases = $(wanman1CasesElement).text();

            let wanman2CasesElement = $("tr:contains('MidCentral') td:nth-of-type(2)");
            let wanman2Cases = $(wanman2CasesElement).text();

            let manwanCases = parseInt(wanman1Cases) + parseInt(wanman2Cases);
            console.log(`manwanCases: ${manwanCases}`);


            let northlandCasesElement = $("tr:contains('Northland') td:nth-of-type(2)");
            let northlandCases = $(northlandCasesElement).text();
            console.log(`northlandCases: ${northlandCases}`);

            let taranakiCasesElement = $("tr:contains('Taranaki') td:nth-of-type(2)");
            let taranakiCases = $(taranakiCasesElement).text();
            console.log(`Taranaki: ${taranakiCases}`);

            let wkt1CasesElement = $("tr:contains('Waikato') td:nth-of-type(2)");
            let wkt1Cases = $(wkt1CasesElement).text();

            let wkt2CasesElement = $("tr:contains('Lakes') td:nth-of-type(2)");
            let wkt2Cases = $(wkt2CasesElement).text();

            let waikatoCases = parseInt(wkt1Cases) + parseInt(wkt2Cases);
            console.log(`waikatoCases: ${waikatoCases}`);

            let wlt1CasesElement = $("tr:contains('Hutt Valley') td:nth-of-type(2)");
            let wlt1Cases = $(wlt1CasesElement).text();

            let wlt2CasesElement = $("tr:contains('Capital and Coast') td:nth-of-type(2)");
            let wlt2Cases = $(wlt2CasesElement).text();

            let wlt3CasesElement = $("tr:contains('Wairarapa') td:nth-of-type(2)");
            let wlt3Cases = $(wlt3CasesElement).text();

            let wellingtonCases = parseInt(wlt1Cases) + parseInt(wlt2Cases) + parseInt(wlt3Cases);
            console.log(`wellingtonCases: ${wellingtonCases}`);


            console.log("---------------------");

            let can1CasesElement = $("tr:contains('Canterbury') td:nth-of-type(2)");
            let can1Cases = $(can1CasesElement[0]).text();
            let can2Cases = $(can1CasesElement[1]).text();

            let canterburyCases = parseInt(can1Cases) + parseInt(can2Cases);
            console.log(`canterburyCases: ${canterburyCases}`);


            let nelsonCasesElement = $("tr:contains('Nelson Marlborough') td:nth-of-type(2)");
            let nelsonCases = $(nelsonCasesElement).text();
            console.log(`nelsonCases: ${nelsonCases}`);

            let southCasesElement = $("tr:contains('Southern') td:nth-of-type(2)");
            let southlandCases = $(southCasesElement).text();
            console.log(`southlandCases: ${southlandCases}`);


            let westcoastCasesElement = $("tr:contains('West Coast') td:nth-of-type(2)");
            let westcoastCases = $(westcoastCasesElement).text();
            console.log(`westcoastCases: ${westcoastCases}`);



            // let totalCasesChartImageURL = host + $("img[alt='Total cases of COVID-19 by DHB, at 30 March']").attr('src');
            // console.log(totalCasesChartImageURL);

            // let totalCasesMapImageURL = host + $("img[alt='Map of cases by DHB']").attr('src');
            // console.log(totalCasesMapImageURL);

            // let totalCasesAgeImageURL = host + $("img[alt='Total cases of COVID-19 in NZ by age']").attr('src');
            // console.log(totalCasesAgeImageURL);

            // let totalCasesSexImageURL = host + $("img[alt='Total cases of COVID-19 by sex at 30 March']").attr('src');
            // console.log(totalCasesSexImageURL);

            // let totalCasesEthnicityImageURL = host + $("img[alt='Ethnicity percentages of COVID-19 cases by sex at 29 March']").attr('src');
            // console.log(totalCasesEthnicityImageURL);

            let d = new Date();
            let dt = d.toISOString().substr(5, 5);// 03-25

            console.log(`----------date is: ${dt}-----------`);
            console.log(`----------date is: ${d.toISOString()}-----------`);

            if (!isNaN(newCases) && !isNaN(totalCases) && !isNaN(recoveredCases) && !isNaN(deathCases)) {
                console.log("----------dayUpdate-----------");

                let dayUpdate = db.collection('dayUpdate').doc(dt);
                let result = dayUpdate.set({
                    id: dt,
                    newCases: parseInt(newCases),
                    totalCases: parseInt(totalCases),
                    recoveredCases: parseInt(recoveredCases),
                    deathCases: parseInt(deathCases),
                });


                let moreUpdate = db.collection('more').doc(dt);
                let moreResult = moreUpdate.set({
                    id: dt,
                    numsNew: parseInt(newCases),
                    numsAll: parseInt(totalCases),
                    numsCured: parseInt(recoveredCases),
                    numsDeath: parseInt(deathCases),
                });
                console.log(`----------dayUpdate result ${result}-----------`);
                console.log(`----------moreUpdate result ${moreResult}-----------`);
            }

            let regions = ['auckland', 'bayofplenty', 'canterbury', 'gisborne', 'hawkesbay', 'manwan', 'nelson', 'northland', 'southland', 'taranaki', 'waikato', 'wellington', 'westcoast']

            let exe = regions.map(region => {
                let regionCases = region + 'Cases';
                let regionNum = eval(regionCases);
                console.log(`----------regionUpdate ${regionCases}: ${regionNum}-----------`);

                if (!isNaN(regionNum)) {
                    let regionUpdate = db.collection('regionUpdate').doc(region);
                    let result = regionUpdate.set({
                        totalCases: parseInt(regionNum),
                        updateDate: dt
                    });

                    let casesUpdate = db.collection('cases').doc(region);
                    let casesResult = casesUpdate.set({
                        numsAll: parseInt(regionNum),
                        date: dt,
                        id: region
                    });

                    console.log(`----------regionUpdate result ${result}-----------`);
                    console.log(`----------casesUpdate result ${casesResult}-----------`);
                }
            });

            console.log(`----------regionUpdate exe ${exe}-----------`);

        }
        done();
    }
});

// Queue just one URL, with default callback
c.queue(`${host}/our-work/diseases-and-conditions/covid-19-novel-coronavirus/covid-19-current-situation/covid-19-current-cases`);